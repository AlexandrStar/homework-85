import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';

export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});

export const fetchAlbums = () => {
  return dispatch => {
    return axios.get('/albums').then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    );
  };
};

export const fetchAlbum = id => {
  return dispatch => {
    return axios.get('/albums/' + id)
      .then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    );
  };
};

export const createAlbum = albumData => {
  return dispatch => {
    return axios.post('/albums', albumData).then(
      () => {
        dispatch(createAlbumSuccess());
        NotificationManager.success('You added new album');
      }
    );
  };
};

export const publishAlbum = (id, publish, artistId) => {
  return dispatch => {
    return axios.post(`/albums/${id}/toggle_published`, publish).then(
      () => {
        dispatch(fetchAlbum(artistId));
        NotificationManager.success('You published an album!');
      }
    );
  };
};

export const removedAlbum = (id, remove, artistId) => {
  return dispatch => {
    return axios.post(`/albums/${id}/toggle_removed`, remove).then(
      () => {
        dispatch(fetchAlbum(artistId));
        NotificationManager.success('You removed an album!');
      }
    );
  };
};

