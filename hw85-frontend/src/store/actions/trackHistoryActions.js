import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";

export const TRACK_HISTORY_SUCCESS = 'TRACK_HISTORY_SUCCESS';

const fetchTrackHistorySuccess = trackHistory => ({type: TRACK_HISTORY_SUCCESS, trackHistory});

export const fetchTrackHistory = trackData => {
  return (dispatch, getState) => {
    const user = getState().users.user;
    if (!user) {
      dispatch(push('/login'))
    } else {
      return axios.post('/track_history', trackData, {headers: {'Authorization': user.token}}).then(
        response => {
          dispatch(fetchTrackHistorySuccess());
          NotificationManager.success('You listened to the song!');
        }
      );
    }
  };
};

export const trackHistory = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    return axios.get('/track_history', {headers: {'Authorization': token}}).then(
      response => {
        dispatch(fetchTrackHistorySuccess(response.data));
      }
    )
  }
};
