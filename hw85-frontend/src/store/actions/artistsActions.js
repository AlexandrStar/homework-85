import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';

export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});
export const createArtistsSuccess = () => ({type: CREATE_ARTIST_SUCCESS});

export const fetchArtist = () => {
  return dispatch => {
    return axios.get('/artists').then(
      response => dispatch(fetchArtistsSuccess(response.data))
    );
  };
};

export const createArtist = artistData => {
  return dispatch => {
    return axios.post('/artists', artistData).then(
      () => {
        dispatch(createArtistsSuccess());
        NotificationManager.success('You added new artist!');
      }
    );
  };
};

export const publishArtist = (id, publish) => {
  return dispatch => {
    return axios.post(`/artists/${id}/toggle_published`, publish).then(
      () => {
        dispatch(fetchArtist());
        NotificationManager.success('You published an artist!');
      }
    );
  };
};

export const removedArtist = (id, remove) => {
  return dispatch => {
    return axios.post(`/artists/${id}/toggle_removed`, remove).then(
      () => {
        dispatch(fetchArtist());
        NotificationManager.success('You removed an artist!');
      }
    );
  };
};

