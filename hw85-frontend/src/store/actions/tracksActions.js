import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';

export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});
export const createTracksSuccess = () => ({type: CREATE_TRACK_SUCCESS});

export const fetchTrack = id => {
  return dispatch => {
    return axios.get('/tracks?album=' + id)
      .then(
        response => dispatch(fetchTracksSuccess(response.data))
      );
  };
};

export const createTrack = trackData => {
  return dispatch => {
    return axios.post('/tracks', trackData).then(
      () => {
        dispatch(createTracksSuccess());
        NotificationManager.success('You added new track!');
      }
    );
  };
};

export const publishTrack = (id, publish, albumId) => {
  return dispatch => {
    return axios.post(`/tracks/${id}/toggle_published`, publish).then(
      () => {
        dispatch(fetchTrack(albumId));
        NotificationManager.success('You published an track!');
      }
    );
  };
};

export const removedTrack = (id, remove, albumId) => {
  return dispatch => {
    return axios.post(`/tracks/${id}/toggle_removed`, remove).then(
      () => {
        dispatch(fetchTrack(albumId));
        NotificationManager.success('You removed an track!');
      }
    );
  };
};

